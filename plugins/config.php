<?php
/*	This file is part of 3way-uploader.

	Copyright Voker57 2008

	3way-uploader is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	3way-uploader is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with 3way-uploader.  If not, see <http://www.gnu.org/licenses/>.
*/
 require("shoo.php");
session_start();
// get rid of stupid magic quotes
 if (get_magic_quotes_gpc()) {
    function stripslashes_deep($value)
    {
        $value = is_array($value) ?
                    array_map('stripslashes_deep', $value) :
                    stripslashes($value);

        return $value;
    }

    $_POST = array_map('stripslashes_deep', $_POST);
    $_GET = array_map('stripslashes_deep', $_GET);
    $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
    $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
}
// this may be useful in localconfig
function size_readable($size, $unit = null, $retstring = null, $si = false)
 {
     // Units
     if ($si === true) {
         $sizes = array('B', 'kB', 'MB', 'GB', 'TB', 'PB');
         $mod   = 1000;
     } else {
         $sizes = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
         $mod   = 1024;
     }
     $ii = count($sizes) - 1;

     // Max unit
     $unit = array_search((string) $unit, $sizes);
     if ($unit === null || $unit === false) {
         $unit = $ii;
     }

     // Return string
     if ($retstring === null) {
         $retstring = '%01.2f %s';
     }

     // Loop
     $i = 0;
     while ($unit != $i && $size >= 1024 && $i < $ii) {
         $size /= $mod;
         $i++;
     }

     return sprintf($retstring, $size, $sizes[$i]);
 }

 function mkdir_perms($dr, $pr)
{
	mkdir($dr);
	chmod($dr,$pr);
}


$r=dirname($_SERVER['PHP_SELF']);
if($r=="/") $r="";
$siteroot=$_SERVER['SERVER_NAME'];
require_once("localconfig.php");
?>