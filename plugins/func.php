<?php

/*	This file is part of 3way-uploader.

	Copyright Voker57 2008

	3way-uploader is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	3way-uploader is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with 3way-uploader.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once("config.php");
require_once("shoo.php");

function err_inf($what)
{
	$_SESSION['inf_err']=$_SESSION['inf_err'].$what." <br />";
}

function gen_id($length=7)
{
	$unvowels=str_split("wrtpsdfghjklzxcvbnm");
	$vowels=str_split("eyuioa");
	$id = array();
	for($i=0; $i < $length; $i++)
	{
		if($i%2 == 0) {
			$id[] = $unvowels[array_rand($unvowels)];
		} else {
			$id[] = $vowels[array_rand($vowels)];
		}
	}
	return join($id);
}

function inf_err()
{
	if(isset($_SESSION['inf_err']))
	{
		echo $_SESSION['inf_err'];
		unset($_SESSION['inf_err']);
	}
}

function redir($where)
{
	header("Location: $where");
	die();
}

?>