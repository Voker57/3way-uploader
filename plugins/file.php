<?php
/*	This file is part of 3way-uploader.

	Copyright Voker57 2008

	3way-uploader is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	3way-uploader is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with 3way-uploader.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once("shoo.php");

function nFileHandleUpload($file)
{
	global $nFilePrefix, $filelimit;
	$name = $file['name'];
	$tmp_name = $file['tmp_name'];
	if($file['size']>$filelimit) return 0;
	$name=preg_replace("{/|\\s+}","_",$name);
	$name=preg_replace("/^\\./","0.", $name);
	$name=preg_replace("/^index/","noindex", $name);
	$name=preg_replace("/^default/","nodefault", $name);
	do
	{
		$pref = gen_id();
	} while(is_file("$nFilePrefix/$pref/$name"));
	$name = "$pref/$name";
	if(!is_dir("$nFilePrefix/$pref")) mkdir_perms("$nFilePrefix/$pref",0755);
	if(!is_dir("$nFilePrefix/html/$pref")) mkdir_perms("$nFilePrefix/html/$pref",0755);
	move_uploaded_file($tmp_name,"$nFilePrefix/$name");
	chmod("$nFilePrefix/$name",0755) or $fail=1;
	if ($fail==1) return 0; else {
		$_SESSION['own'][]="$nFilePrefix/$name";
	 	return $name;
	 }
}
?>
