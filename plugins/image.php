<?php
/*	This file is part of 3way-uploader.

	Copyright Voker57 2008

	3way-uploader is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	3way-uploader is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with 3way-uploader.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once("shoo.php");
$thumbwidth=400;
$thumbheight=300;

function nImageHandleUpload($file)
{
	global $nImgPrefix,$thumbwidth, $thumbheight, $r, $siteroot, $imglimit, $useIM;
	$name = $file['name'];
	$tmpname = $file['tmp_name'];
	if($file['size']>$imglimit) return 0;
	$name=preg_replace("{/|\\s+}","_",$name);
	$name=preg_replace("/\\.\\w+$/","",$name);
	if(!$useIM)
	{
		$size=getimagesize($tmpname);
		switch($size[2])
		{
			case IMAGETYPE_PNG: $imgtype="png"; break;
			case IMAGETYPE_GIF: $imgtype="gif"; break;
			case IMAGETYPE_JPEG: $imgtype="jpeg"; break;
			default: return 0;
		}
		$action="imagecreatefrom$imgtype";
		if(!$image=$action($tmpname)) return 0;
	} else
	{
		if(!preg_match("/^\\S+ ([A-Z0-9]+) \\d+x\\d+\\S+ .*$/", exec("identify $tmpname"), $matches))
		{
			err_inf("Not an supported image");
			return 0;
		}
		$imgtype=strtolower($matches[1]);
	}
	$name="$name.$imgtype";
	if(!in_array($imgtype, array('png', 'jpeg', 'gif', 'tiff')))
		return 0;
	do
	{
		$pref=gen_id();
	} while(is_file("$nImgPrefix/$pref/$name"));
	$name="$pref/$name";
	if(!is_dir("$nImgPrefix/$pref")) mkdir_perms("$nImgPrefix/$pref",0755);
	if(!is_dir("$nImgPrefix/html/$pref")) mkdir_perms("$nImgPrefix/html/$pref",0755);
	if(!is_dir("$nImgPrefix/thumbs/$pref")) mkdir_perms("$nImgPrefix/thumbs/$pref",0755);
	move_uploaded_file($tmpname,"$nImgPrefix/$name");
	if(!$useIM)
	{
		if(imagesx($image)>$thumbwidth)
		{
			$thumb=imagecreatetruecolor($thumbwidth,imagesy($image)*$thumbwidth/imagesx($image));
			imagecopyresampled($thumb,$image,0,0,0,0,$thumbwidth,imagesy($image)*$thumbwidth/imagesx($image),imagesx($image),imagesy($image));
		} else $thumb=$image;
		$action="image$imgtype";
		$action($thumb,"$nImgPrefix/thumbs/$name");
	} else
	{
		exec("convert -size ".$thumbwidth."x".$thumbheight." $nImgPrefix/$name -resize ".$thumbwidth."x".$thumbheight." +profile \"*\" $nImgPrefix/thumbs/$name");
	}
	chmod("$nImgPrefix/$name",0755) or $fail=1;
	chmod("$nImgPrefix/thumbs/$name",0755) or $fail=1;
	return $name;
}
?>