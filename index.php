<?php
/*	This file is part of 3way-uploader.

	Copyright Voker57 2008

	3way-uploader is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	3way-uploader is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with 3way-uploader.  If not, see <http://www.gnu.org/licenses/>.
*/
// That's NBL stuff uploading script, ripped from NPE (NBL Php Engine, gotta be released soon)
// Updated.
// Readin plugins
$called=true;
$dirname='plugins';
$p_dir=dir($dirname);
	while(($fname=$p_dir->read()) !== false)
		if(preg_match("/^[0-9a-z]+\.php$/",$fname)) require_once("$dirname/$fname");

// Creating directories if they do not exist
foreach(array("$nImgPrefix/thumbs","$nImgPrefix/html","$nFilePrefix/html",$nTextPrefix) as $cdir)
{
	if(!file_exists($cdir)) mkdir($cdir, 0755, true);
}

if(isset($_FILES['image']) && $imagingEnabled)
{
	$uplImage=nImageHandleUpload($_FILES['image']);
	$fullname = split('/',$uplImage);
	if(!$uplImage) err_inf('Incorrect image('.$_FILES['image']['error'].')');
	else {
	 file_put_contents("$nImgPrefix/html/$uplImage.html", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
	<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='ru' lang='ru'>
	<head>
	<title>$fullname[1]</title>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<link rel='STYLESHEET' href='$r/css.css' type= 'text/css' />
	<link rel='icon' type='image/png' href='$r/favicon.png' />
	</head>
	<body>
	<a href='$r/$nImgPrefix/$uplImage'><img src='$r/$nImgPrefix/thumbs/$uplImage' alt='Image'></a><br />
	<form action='/' method='post'>
	Direct link: <input type='text' size='75' readonly='readonly' name='direct' value='http://$siteroot$r/$nImgPrefix/$uplImage' /><br />
	Thumbnail link: <input type='text' size='75' readonly='readonly' name='directthumb' value='http://$siteroot$r/$nImgPrefix/thumbs/$uplImage' /><br />
	BBCODE: <input type='text' readonly='readonly' size='75' name='phpbb' value='[url=http://$siteroot$r/$nImgPrefix/$uplImage][img]http://$siteroot$r/$nImgPrefix/thumbs/$uplImage"."[/img][/url]' /><br />
	BBCODE (w/o preview): <input type='text' size='75' readonly='readonly' name='phpbb' value='[img]http://$siteroot$r/$nImgPrefix/$uplImage"."[/img]' /><br />
	HTML: <input type='text' size='75' readonly='readonly' name='html' value='<a href=\"http://$siteroot$r/$nImgPrefix/$uplImage\"> <img src=\"http://$siteroot$r/$nImgPrefix/thumbs/$uplImage\" alt=\"NBL Image\" /> </a>' /> <br />
	HTML (w/o preview): <input type='text' size='75' readonly='readonly' name='html' value='<img src=\"http://$siteroot$r/$nImgPrefix/$uplImage\" alt=\"NBL Image\" />' /> <br />
	Textile: <input type='text' size='75' readonly='readonly' name='html' value='!http://$siteroot$r/$nImgPrefix/thumbs/$uplImage!:http://$siteroot$r/$nImgPrefix/$uplImage' /> <br />
	Textile (w/o preview): <input type='text' size='75' readonly='readonly' name='html' value='!http://$siteroot$r/$nImgPrefix/$uplImage!' /> <br />
	</form>

	<form action='$r/' method='post' enctype='multipart/form-data'>
	<input type='hidden' name='MAX_FILE_SIZE' value='$imglimit' />
	<input type='file' size='50' name='image' /> <br />
	<input type='submit' value='Dump another one!' />

	<a href='$r/'>NBL file dumping facility</a>
	</body>
	</html>");
	chmod("$nImgPrefix/html/$uplImage.html",0755);
	if($_POST['simple'])
		redir("$r/$nImgPrefix/$uplImage");
	elseif($useHtaccess)
		redir("$r/i/$uplImage");
	else
		redir("$r/$nImgPrefix/html/$uplImage.html");
	}
} elseif(isset($_FILES['file']) && $filesEnabled)
{
	$uplFile=nFileHandleUpload($_FILES['file']);
	$about = preg_replace("/^.*:([^:]+)$/","$1", exec("file --mime $nFilePrefix/$uplFile"));
	if(!$uplFile) err_inf("Something is wrong. ".$_FILES['image']['error']);
	else
	{
		$fullname = split('/',$uplFile);
	file_put_contents("$nFilePrefix/html/$uplFile.html",
	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
	<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='ru' lang='ru'>
	<head>
	<title>$fullname[1]</title>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
	<link rel='icon' type='image/png' href='$r/favicon.png' />
	<link rel='STYLESHEET' href='$r/css.css' type= 'text/css' />
	</head>
	<body><p>
	$fullname[1], $about, ".size_readable(filesize("$nFilePrefix/$uplFile"),null,'%d %s')."
	<br />
	<a href='http://$siteroot$r/$nFilePrefix/$uplFile'>Download</a></p>
	<form action='$r/' method='post' enctype='multipart/form-data'>
	<input type='hidden' name='MAX_FILE_SIZE' value='$filelimit' />
	<input type='file' size='50' name='file' /> <br />
	<input type='submit' value='Dump another one!' />
	<a href='$r/'>NBL file dumping facility</a> | <small><a href='$r/?delete=$nFilePrefix/$uplFile'>Delete this file</a></small> </body></html>");
	chmod("$nFilePrefix/html/$uplFile.html",0755) or $fail=1;
	if($_POST['simple'])
		redir("$r/$nFilePrefix/$uplFile");
	elseif($useHtaccess)
		redir("$r/f/$uplFile");
	else
		redir("$r/$nFilePrefix/html/$uplFile.html");
	}
} 
// uploading form
echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='ru' lang='ru'>
<head>
<title>NBL file dumping facility</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='icon' type='image/png' href='$r/favicon.png' />
<link rel='STYLESHEET' href='$r/css.css' type= 'text/css' />
</head>
<body><div class='content'>
";
inf_err();
if($imagingEnabled) echo "
<h2> Dump a picture </h2>
	<form action='$r/' method='post' enctype='multipart/form-data'>
	<input type='hidden' name='MAX_FILE_SIZE' value='$imglimit' />
	<input type='file' size='50' name='image' /> <br />
	<input type='submit' value='Dump!' />
</form>";
if($filesEnabled) echo "
<h2> Dump a file </h2>
	<form action='$r/' method='post' enctype='multipart/form-data'>
	<input type='hidden' name='MAX_FILE_SIZE' value='$filelimit' />
	<input type='file' size='50' name='file' /> <br />
	<input type='submit' value='Dump!' />
</form>";
echo "
</div>
<hr />
$message";
?>
</body>
</html>
